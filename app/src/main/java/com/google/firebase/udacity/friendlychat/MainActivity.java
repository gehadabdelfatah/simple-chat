package com.google.firebase.udacity.friendlychat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.auth.*;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String ANONYMOUS = "anonymous";
    public static final int DEFAULT_MSG_LENGTH_LIMIT = 1000;

    private ListView mMessageListView;
    private MessageAdapter mMessageAdapter;
    private ProgressBar mProgressBar;
    private ImageButton mPhotoPickerButton;
    private EditText mMessageEditText;
    private Button mSendButton;
    private static final int RC_SIGN_IN = 1;
    private static final int RC_PHOTO_PICKER = 2;
    private String mUsername;
    //firebase data
   private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
   private ChildEventListener childEventListener;
    private  FirebaseAuth firebaseAuth;
    private  FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private String FriendlyKeymessage="friendly_message_length";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this,MyFirebaseInstanceIDService.class);
        startService(intent);
        Log.w(TAG, "Refreshed token: " + "added");

        mUsername = ANONYMOUS;
        //Firebase varibles intialze
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth =FirebaseAuth.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        mFirebaseRemoteConfig =FirebaseRemoteConfig.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference().child("messages");
        storageReference=firebaseStorage.getReference().child("chat_photos");
        // Initialize references to views
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessageListView = (ListView) findViewById(R.id.messageListView);
        mPhotoPickerButton = (ImageButton) findViewById(R.id.photoPickerButton);
        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
        mSendButton = (Button) findViewById(R.id.sendButton);

        // Initialize message ListView and its adapter
        List<FriendlyMessage> friendlyMessages = new ArrayList<>();
        mMessageAdapter = new MessageAdapter(this, R.layout.item_message, friendlyMessages);
        mMessageListView.setAdapter(mMessageAdapter);

        // Initialize progress bar
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);

        // ImagePickerButton shows an image picker to upload a image for a message
        mPhotoPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                //only jpj photo
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                // look here
                startActivityForResult(Intent.createChooser(intent,"Complete Action Using"),RC_PHOTO_PICKER);

            }
        });

        // Enable Send button when there's text to send
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //text or photo url is null and here photurl is null
                 FriendlyMessage friendlyMessage = new FriendlyMessage(mMessageEditText.getText().toString(), mUsername, null);
                mDatabaseReference.push().setValue(friendlyMessage);

                // Clear input box
                mMessageEditText.setText("");
            }
        });

        authStateListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    //that user is sign in
                  //  Toast.makeText(MainActivity.this,"You are sign in now ",Toast.LENGTH_LONG).show();
                    signinintialze(firebaseUser.getDisplayName());
                }else {
                    //that user is signout
                    signoutintailze();
                    //do that before on activity result
                    startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                            .setProviders(AuthUI.EMAIL_PROVIDER,AuthUI.GOOGLE_PROVIDER)
                            .setIsSmartLockEnabled(false)
                            .build(),RC_SIGN_IN);

                }
            }
        };
        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        //is true now
        Log.i(TAG, "Buildconfigis " + BuildConfig.DEBUG);
        mFirebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
        HashMap<String ,Object> defaultconfig=new HashMap<>();
//add parameter for remote config
        defaultconfig.put(FriendlyKeymessage, DEFAULT_MSG_LENGTH_LIMIT);
        mFirebaseRemoteConfig.setDefaults(defaultconfig);
        fetchConfig();
    }

    private void fetchConfig() {
        long cacheExpiration=3600;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration=0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mFirebaseRemoteConfig.activateFetched();
                applyretrievelimitMethod();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG, "Fetch is faluire");
         applyretrievelimitMethod();
            }
        });  }
        public void applyretrievelimitMethod (){
            //see here
            /*Integer id=7878;
            int idj=id.intValue();*/
            Long message_length = mFirebaseRemoteConfig.getLong(FriendlyKeymessage);
            mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(message_length.intValue())});
            Log.i(TAG, "friendly message length is " + message_length);
            System.out.println("Message");

        }



    //call that before onresume so before startactivitlyforresult
    //call that function in two case:in sign in and in when user click picker photo
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
          Toast.makeText(MainActivity.this,"Signed in ",Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
               Toast.makeText(MainActivity.this,"Signed in is canceled",Toast.LENGTH_LONG).show();
               finish();
            }
            //should check resultCode
        } else if (requestCode == RC_PHOTO_PICKER && resultCode==RESULT_OK) {
            Uri selectedImageuri=data.getData();
          //get reference to store file to chat_photos/<filename>
            StorageReference PhotoRef=storageReference.child(selectedImageuri.getLastPathSegment());
           //upload file to firbase storage
            PhotoRef.putFile(selectedImageuri).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri dowloadUri = taskSnapshot.getDownloadUrl();
                    //text or photo url is null and here text is null
                    //here i save photourl of database in firebase
                    FriendlyMessage friendlyMessage = new FriendlyMessage(null, mUsername, dowloadUri.toString());
                    mDatabaseReference.push().setValue(friendlyMessage);

                }
            });
        }
    }
//that function add message to list after its record in firbasedatabase
    private void signinintialze(String username) {
        mUsername=username;
        if (childEventListener == null) {


        childEventListener=new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                FriendlyMessage  friendlyMessage= dataSnapshot.getValue(FriendlyMessage.class);
                mMessageAdapter.add(friendlyMessage);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabaseReference.addChildEventListener(childEventListener);
        }
    }

    private void signoutintailze() {
        mUsername=ANONYMOUS;
        mMessageAdapter.clear();
       detacheddatabaselistner();
    }

  private   void detacheddatabaselistner() {
        if (childEventListener != null) {
            mDatabaseReference.removeEventListener(childEventListener);
            childEventListener = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
        detacheddatabaselistner();
        mMessageAdapter.clear();

    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId() ) {
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(this);
            return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
